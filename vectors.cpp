#include<iostream>
#include<vector>
#include<iomanip>
#include<string>
#include<conio.h>
#include "Textable.h"
#include <windows.h>

using namespace std;
//Funcion de ayuda
void myprint(const wchar_t *str)
{
    WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), str, wcslen(str), NULL, NULL);
}

bool checkCategoryOption(int option);
bool checkMinYear(int year);
bool checkForItemInt(vector<int> arr, int data);
bool checkForItemInt(vector<string> arr, string data);
void printTable(vector<int> taxis, vector<string> plates, vector<string> engines, vector<string> models,
vector<int> years, vector<int> categories, vector<string> driversName, vector<string> driversLastName,
vector<string> driversID, vector<int> driversInsurance, vector<int> driversPhone);

void printTableQueue(vector<int> taxis, vector<string> plates, vector<string> engines, vector<string> models,
vector<int> years, vector<int> categories, vector<string> driversName, vector<string> driversLastName,
vector<string> driversID, vector<int> driversInsurance, vector<int> driversPhone, vector<string> costumersName,
vector<string> departures, vector<string>arrivals, vector<double> costs);

int main(){
	char response='\0';
	int option;
	int categoryOption;

	int idToReinsert;

	// Datos individuales a solicitar	
	int id;
	string plate;
	string engine;
	string model;
	int year;
	int category;
	string driverName;
	string driverLastName;
	string driverID;
	int driverInsurance;
	int driverPhone;
	string customerName;
	string departure;
	string arrival;
	double cost;
	
	//Vectores que almacenan cola en espera
	vector<int> taxis;
	vector<string> plates;
	vector<string> engines;
	vector<string> models;
	vector<int> years;
	vector<int> categories;
	vector<string> driversName;
	vector<string> driversLastName;
	vector<string> driversID;
	vector<int> driversInsurance;
	vector<int> driversPhone;

	//Vectores que almacenan rutas activas
	vector<int> ntaxis;
	vector<string> nplates;
	vector<string> nengines;
	vector<string> nmodels;
	vector<int> nyears;
	vector<int> ncategories;
	vector<string> ndriversName;
	vector<string> ndriversLastName;
	vector<string> ndriversID;
	vector<int> ndriversInsurance;
	vector<int> ndriversPhone;
	vector<string> customersName;
	vector<string> departures;
	vector<string> arrivals;
	vector<double> costs;
	
	do{
		system("cls");
		cout<<"Opciones principales:\n\n"<<endl;
		cout<<"\t\t1. Agregar un nuevo taxi"<<endl;
		cout<<"\t\t2. Lista de taxis en espera"<<endl;
		cout<<"\t\t3. Enviar taxi a cliente"<<endl;
		cout<<"\t\t4. Lista de taxis en ruta"<<endl;
		cout<<"\t\t5. Reinsertar un taxi a la cola de espera"<<endl;
		cout<<"\t\t6. Salir del programa"<<endl<<endl;
		cout<<"Please, input your option: ";
		cin>>option;
		switch(option){
			case 1:
				do{
					cout<<"Correlativo unico: ";
					cin>>id;
				}while(checkForItemInt(taxis, id));
				taxis.push_back(id);
				
				do {
					cout<<"Placa: ";
					cin>>plate;
				}while(checkForItemInt(plates, plate));
				plates.push_back(plate);
				
				do {
					cout<<"Numero de motor: ";
					cin>>engine;
				}while(checkForItemInt(engines, engine));
				engines.push_back(engine);
				
				cout<<"Modelo: ";
				cin.ignore();
				getline(cin,model);
				models.push_back(model);
				
				do {
					myprint(L"Año: ");
					cin>>year;
				}while(checkMinYear(year));
				
				
				if (year>=2015) {
					categories.push_back(1);
				} else {
					categories.push_back(2);
				}
				
				years.push_back(year);
				
				
				cout<<"Nombre del conductor: ";
				cin.ignore();
				getline(cin,driverName);
				driversName.push_back(driverName);
				
				cout<<"Apellido del conductor: ";
				getline(cin,driverLastName);
				driversLastName.push_back(driverLastName);
				
				do {
					cout<<"Numero de Documento de Identidad: ";
					cin>>driverID;
				}while(checkForItemInt(driversID, driverID));
				driversID.push_back(driverID);
				
				do {
					cout<<"Numero de Seguro Social: ";
					cin>>driverInsurance;
				}while(checkForItemInt(driversInsurance, driverInsurance));
				driversInsurance.push_back(driverInsurance);
				
				do {
					cout<<"Numero de telefono: ";
					cin>>driverPhone;
				}while(checkForItemInt(driversPhone, driverPhone));
				driversPhone.push_back(driverPhone);
				
			break;
			case 2:		
				printTable(taxis, plates, engines, models, years, categories, driversName, 
				driversLastName, driversID, driversInsurance, driversPhone);
			break;
			case 3:
				do{
					cout<<"Seleccione una categoria: \n";
					cout<<"\t1. Ejecutiva"<<endl;
					cout<<"\t2. Tradicional"<<endl;
					cin>>categoryOption;
				} while(!checkCategoryOption(categoryOption));
				
				if (categories.size() > 0 ) {
					for(int i=0; i<categories.size(); i++){
						if (categories.at(i) == categoryOption) {
							cout<<"Nombre del cliente: ";
							cin.ignore();
							getline(cin,customerName);
							customersName.push_back(customerName);
							
							cout<<"Origen: ";
							getline(cin,departure);
							departures.push_back(departure);
							
							cout<<"Destino: ";
							getline(cin,arrival);
							arrivals.push_back(arrival);
							
							cout<<"Costo del viaje USD: ";
							cin>>cost;
							costs.push_back(cost);
							
							ntaxis.push_back(taxis.at(i));
							nplates.push_back(plates.at(i));
							nengines.push_back(engines.at(i));
							nmodels.push_back(models.at(i));
							nyears.push_back(years.at(i));
							ncategories.push_back(categories.at(i));
							ndriversName.push_back(driversName.at(i));
							ndriversLastName.push_back(driversLastName.at(i));
							ndriversID.push_back(driversID.at(i));
							ndriversInsurance.push_back(driversInsurance.at(i));
							ndriversPhone.push_back(driversPhone.at(i));
							
							taxis.erase(taxis.begin()+i);
							plates.erase(plates.begin()+i);
							engines.erase(engines.begin()+i);
							models.erase(models.begin()+i);
							years.erase(years.begin()+i);
							categories.erase(categories.begin()+i);
							driversName.erase(driversName.begin()+i);
							driversLastName.erase(driversLastName.begin()+i);
							driversID.erase(driversID.begin()+i);
							driversInsurance.erase(driversInsurance.begin()+i);
							driversPhone.erase(driversPhone.begin()+i);
							
							cout<<"Taxi asignado a ruta correctamente \n";
							break;
						} else {
							if (i == categories.size() - 1 ) {
								cout<<"Sin taxis disponibles \n";			
							}
						}
					}
				} else {
					cout<<"Sin taxis disponibles \n";
				}
			break;
			case 4:
				printTableQueue(ntaxis, nplates, nengines, nmodels, nyears, ncategories, ndriversName, 
				ndriversLastName, ndriversID, ndriversInsurance, ndriversPhone, customersName, departures,
				arrivals, costs);
			break;
			case 5:
				cout<<"Lista de taxis en ruta: "<<endl;
				printTableQueue(ntaxis, nplates, nengines, nmodels, nyears, ncategories, ndriversName, 
				ndriversLastName, ndriversID, ndriversInsurance, ndriversPhone, customersName, departures,
				arrivals, costs);
				cout<<"Ingresa el numero de orden que quieres reinsertar: "<<endl;
				cin>>idToReinsert;
				
				taxis.push_back(ntaxis.at(idToReinsert));
				plates.push_back(nplates.at(idToReinsert));
				engines.push_back(nengines.at(idToReinsert));
				models.push_back(nmodels.at(idToReinsert));
				years.push_back(nyears.at(idToReinsert));
				categories.push_back(ncategories.at(idToReinsert));
				driversName.push_back(ndriversName.at(idToReinsert));
				driversLastName.push_back(ndriversLastName.at(idToReinsert));
				driversID.push_back(ndriversID.at(idToReinsert));
				driversInsurance.push_back(ndriversInsurance.at(idToReinsert));
				driversPhone.push_back(ndriversPhone.at(idToReinsert));
				
				ntaxis.erase(ntaxis.begin()+idToReinsert);
				nplates.erase(nplates.begin()+idToReinsert);
				nengines.erase(nengines.begin()+idToReinsert);
				nmodels.erase(nmodels.begin()+idToReinsert);
				nyears.erase(nyears.begin()+idToReinsert);
				ncategories.erase(ncategories.begin()+idToReinsert);
				ndriversName.erase(ndriversName.begin()+idToReinsert);
				ndriversLastName.erase(ndriversLastName.begin()+idToReinsert);
				ndriversID.erase(ndriversID.begin()+idToReinsert);
				ndriversInsurance.erase(ndriversInsurance.begin()+idToReinsert);
				ndriversPhone.erase(ndriversPhone.begin()+idToReinsert);
				customersName.erase(customersName.begin()+idToReinsert);
				departures.erase(departures.begin()+idToReinsert);
				arrivals.erase(arrivals.begin()+idToReinsert);
				costs.erase(costs.begin()+idToReinsert);
				cout<<"Taxi ha sido agregado al final de la cola de espera."<<endl;	
			break;
			case 6:
				cout<<"Programa terminado ...";
				system("pause");
				system("exit");
			break;
			default:
				cout<<"Seleccione una opcion correcta."<<endl;
			break;
		}	
  	cout<<"�Quiere continuar? (S/N)?: ";
  	cin>>response;
	}while(response=='S' || response=='s');	
}

bool checkMinYear(int year) {
	if (year>=2010) {
		return false;
	} else {
		cout<<"No es posible agregar vehiculos menor al 2010. \n";
		return true;
	}
}

bool checkCategoryOption(int option) {
	if (option == 1 || option == 2) {
		return true;
	}
	return false;
}

bool checkForItemInt(vector<int> arr, int data){
	for(int i=0;i<arr.size();i++)
	{
		if(arr.at(i)==data)
		{
			cout<<"Data is already registered at position ["<<i<<"].\nPlease Enter Data again.\n";
			return true;
		}
		else
		{
			return false;
		}
	}		
}

bool checkForItemInt(vector<string> arr, string data){
	for(int i=0;i<arr.size();i++)
	{
	if(arr.at(i)==data)
		{
			cout<<"Data is already registered at position ["<<i<<"].\nPlease Enter Data again.\n";
			return true;
		}
		else
		{
			return false;
		}
	}		
}

void printTable(vector<int> taxis, vector<string> plates, vector<string> engines, vector<string> models,
vector<int> years, vector<int> categories, vector<string> driversName, vector<string> driversLastName,
vector<string> driversID, vector<int> driversInsurance, vector<int> driversPhone) {
	TextTable t( '-', '|', '+' );
	t.add("Orden");
	t.add("Correlativo");
	t.add("Placa");
	t.add("Motor");
	t.add("Modelo");
	t.add("Anio");
	t.add("Categoria");
	t.add("Nombre");
	t.add("Apellido");
	t.add("Documento");
	t.add("Seguro");
	t.add("Telefono");
	t.endOfRow();
	for(int i=0; i<taxis.size();i++)
	{
		t.add(to_string(i));
		t.add(to_string(taxis.at(i)));
		t.add(plates.at(i));
		t.add(engines.at(i));
		t.add(models.at(i));
		t.add(to_string(years.at(i)));
		if(categories.at(i) == 1){
			t.add("Ejecutiva");
		} else {
			t.add("Tradicional");
		}
		t.add(driversName.at(i));
		t.add(driversLastName.at(i));
		t.add(driversID.at(i));
		t.add(to_string(driversInsurance.at(i)));
		t.add(to_string(driversPhone.at(i)));
		t.endOfRow();	
	}
	t.setAlignment( 2, TextTable::Alignment::RIGHT );
	cout<<t;
}

void printTableQueue(vector<int> taxis, vector<string> plates, vector<string> engines, vector<string> models,
vector<int> years, vector<int> categories, vector<string> driversName, vector<string> driversLastName,
vector<string> driversID, vector<int> driversInsurance, vector<int> driversPhone, vector<string> costumersName,
vector<string> departures, vector<string>arrivals, vector<double> costs) {
	TextTable t( '-', '|', '+' );
	t.add("Orden");
	t.add("Correlativo");
	t.add("Placa");
	t.add("Modelo");
	t.add("Anio");
	t.add("Categoria");
	t.add("Conductor");
	t.add("Documento");
	t.add("Telefono");
	t.add("Cliente");
	t.add("Origen");
	t.add("Destino");
	t.add("Costo USD");
	t.endOfRow();
	for(int i=0; i<taxis.size();i++)
	{
		t.add(to_string(i));
		t.add(to_string(taxis.at(i)));
		t.add(plates.at(i));
		t.add(models.at(i));
		t.add(to_string(years.at(i)));
		if(categories.at(i) == 1){
			t.add("Ejecutiva");
		} else {
			t.add("Tradicional");
		}
		t.add(driversLastName.at(i));
		t.add(driversID.at(i));
		t.add(to_string(driversPhone.at(i)));
		t.add(costumersName.at(i));
		t.add(departures.at(i));
		t.add(arrivals.at(i));
		t.add(to_string(costs.at(i)));
		t.endOfRow();	
	}
	t.setAlignment( 2, TextTable::Alignment::RIGHT );
	cout<<t;
}
